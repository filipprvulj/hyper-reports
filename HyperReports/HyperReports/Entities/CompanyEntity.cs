﻿using HyperReports.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace HyperReports.Entitites
{
    public class CompanyEntity : BaseEntity
    {
        public string Uuid { get; set; }

        public string CompanyName { get; set; }

        public string Address { get; set; }

        [DoNotPopulate]
        public StoreEntity[] Stores { get; set; }
    }
}