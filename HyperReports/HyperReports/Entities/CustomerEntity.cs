﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HyperReports.Entitites
{
    public class CustomerEntity : BaseEntity
    {
        public string Uuid { get; set; }

        public string Name { get; set; }

        public string Address { get; set; }
    }
}