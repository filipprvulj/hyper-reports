﻿using HyperReports.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace HyperReports.Entitites
{
    public abstract class BaseEntity
    {
        [DoNotInsert]
        public int Id { get; set; }
    }
}