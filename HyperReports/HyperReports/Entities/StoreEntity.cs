﻿using HyperReports.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace HyperReports.Entitites
{
    public class StoreEntity : BaseEntity
    {
        public string Name { get; set; }

        public string Address { get; set; }

        [DoNotPopulate]
        public ReceiptEntity[] Receipts { get; set; }

        [DoNotPopulate]
        public InvoiceEntity[] Invoices { get; set; }

        public int CompanyId { get; set; }

        [DoNotPopulate]
        public CompanyEntity Company { get; set; }
    }
}