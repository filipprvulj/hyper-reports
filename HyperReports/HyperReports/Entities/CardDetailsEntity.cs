﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HyperReports.Entitites
{
    public class CardDetailsEntity : BaseEntity
    {
        public string CardType { get; set; }

        public string CardNumber { get; set; }

        public bool IsContactless { get; set; }
    }
}