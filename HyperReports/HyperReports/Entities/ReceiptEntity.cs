﻿using HyperReports.Attributes;
using System;
using System.Collections.Generic;
using System.Text;
using static HyperReports.Shared.PaymentMethods;

namespace HyperReports.Entitites
{
    public class ReceiptEntity : BaseEntity
    {
        public decimal Total { get; set; }

        public DateTime ReceiptDateTime { get; set; }

        public Payment Payment { get; set; }

        public int? CardDetailsId { get; set; }

        [DoNotPopulate]
        public CardDetailsEntity CardDetails { get; set; }

        public int StoreId { get; set; }

        [DoNotPopulate]
        public StoreEntity Store { get; set; }
    }
}