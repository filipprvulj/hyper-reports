﻿using HyperReports.Attributes;
using System;
using System.Collections.Generic;
using System.Text;
using static HyperReports.Shared.PaymentMethods;

namespace HyperReports.Entitites
{
    public class InvoiceEntity : BaseEntity
    {
        public decimal Total { get; set; }

        public DateTime InvoiceDateTime { get; set; }

        public int CustomerId { get; set; }

        [DoNotPopulate]
        public CustomerEntity Customer { get; set; }

        public Payment Payment { get; set; }

        public int? CardDetailsId { get; set; }

        [DoNotPopulate]
        public CardDetailsEntity CardDetails { get; set; }

        public int StoreId { get; set; }

        [DoNotPopulate]
        public StoreEntity Store { get; set; }
    }
}