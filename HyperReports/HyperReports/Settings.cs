﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Configuration.Json;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace HyperReports
{
    public static class Settings
    {
        private static IConfiguration configuration = new ConfigurationBuilder()
            .SetBasePath(AppContext.BaseDirectory.Substring(0, AppContext.BaseDirectory.IndexOf("bin")))
            .AddJsonFile("appsettings.json")
            .AddJsonFile("appsettings.dev.json")
            .Build();

        public static ConnectionString ConnectionString
        {
            get
            {
                ConnectionString connectionString = new ConnectionString();
                ConfigurationBinder.Bind(configuration, "ConnectionStrings", connectionString);
                return connectionString;
            }
        }

        public static SftpConfig SftpConfig
        {
            get
            {
                SftpConfig sftpConfig = new SftpConfig();
                ConfigurationBinder.Bind(configuration, "SftpConfig", sftpConfig);
                return sftpConfig;
            }
        }

        public static XmlFiles XmlFiles
        {
            get
            {
                XmlFiles xmlFiles = new XmlFiles();
                ConfigurationBinder.Bind(configuration, "XmlFiles", xmlFiles);
                return xmlFiles;
            }
        }

        public static void SetLastFileProcessed(string fileName)
        {
            string jsonFilePath = Path.Combine(AppContext.BaseDirectory.Substring(0, AppContext.BaseDirectory.IndexOf("bin")), @"appsettings.dev.json");
            string json = File.ReadAllText(jsonFilePath);
            JObject jsonObj = JObject.Parse(json);
            JObject xmlFiles = (JObject)jsonObj["XmlFiles"];
            xmlFiles["LastFileProcessed"] = fileName;

            File.WriteAllText(jsonFilePath, jsonObj.ToString());
        }

        public static void SetFileDestination(string path)
        {
            string jsonFilePath = Path.Combine(AppContext.BaseDirectory.Substring(0, AppContext.BaseDirectory.IndexOf("bin")), @"appsettings.dev.json");
            string json = File.ReadAllText(jsonFilePath);
            JObject jsonObj = JObject.Parse(json);
            JObject xmlFiles = (JObject)jsonObj["XmlFiles"];
            xmlFiles["FilePath"] = path;

            File.WriteAllText(jsonFilePath, jsonObj.ToString());
        }

        public static void SetExportPath(string path)
        {
            string jsonFilePath = Path.Combine(AppContext.BaseDirectory.Substring(0, AppContext.BaseDirectory.IndexOf("bin")), @"appsettings.dev.json");
            string json = File.ReadAllText(jsonFilePath);
            JObject jsonObj = JObject.Parse(json);
            JObject xmlFiles = (JObject)jsonObj["XmlFiles"];
            xmlFiles["ExportPath"] = path;

            File.WriteAllText(jsonFilePath, jsonObj.ToString());
        }
    }

    public class ConnectionString
    {
        public string DefaultConnection { get; set; }
    }

    public class SftpConfig
    {
        public string Host { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string HostFingerprint { get; set; }
        public int Port { get; set; }
    }

    public class XmlFiles
    {
        public string FilePath { get; set; }
        public string LastFileProcessed { get; set; }
        public string ExportPath { get; set; }
    }
}