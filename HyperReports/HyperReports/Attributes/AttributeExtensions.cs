﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace HyperReports.Attributes
{
    public static class AttributeExtensions
    {
        public static bool HasAttribute<AttributeType>(this PropertyInfo prop) where AttributeType : Attribute
        {
            var attributes = prop.GetCustomAttributes(typeof(AttributeType));

            return attributes.Any();
        }
    }
}