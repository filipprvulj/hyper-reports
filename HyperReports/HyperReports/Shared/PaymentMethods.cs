﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;

namespace HyperReports.Shared
{
    public class PaymentMethods
    {
        public enum Payment
        {
            [XmlEnum(Name = "cash")]
            Cash,

            [XmlEnum(Name = "card")]
            Card
        }
    }
}