﻿using HyperReports.Attributes;
using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;

namespace HyperReports.Models
{
    [XmlRoot("company")]
    public class CompanyModel : BaseModel
    {
        [XmlAttribute("uuid")]
        public string Uuid { get; set; }

        [XmlAttribute("name")]
        public string CompanyName { get; set; }

        [XmlAttribute("address")]
        public string Address { get; set; }

        [DoNotPopulate]
        [XmlArray("stores"), XmlArrayItem("store")]
        public StoreModel[] Stores { get; set; }
    }
}