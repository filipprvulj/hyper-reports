﻿using HyperReports.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using static HyperReports.Shared.PaymentMethods;

namespace HyperReports.Models
{
    public class InvoiceModel : BaseModel
    {
        [XmlElement("total")]
        public decimal Total { get; set; }

        [XmlElement("datetime")]
        public DateTime InvoiceDateTime { get; set; }

        public int CustomerId { get; set; }

        [DoNotPopulate]
        [XmlElement("customer")]
        public CustomerModel Customer { get; set; }

        [XmlElement("payment")]
        public Payment Payment { get; set; }

        public int CardDetailsId { get; set; }

        [DoNotPopulate]
        [XmlElement("carddetails")]
        public CardDetailsModel CardDetails { get; set; }

        public int StoreId { get; set; }

        [DoNotPopulate]
        public StoreModel Store { get; set; }
    }
}