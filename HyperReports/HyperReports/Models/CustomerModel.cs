﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;

namespace HyperReports.Models
{
    public class CustomerModel : BaseModel
    {
        [XmlElement("uuid")]
        public string Uuid { get; set; }

        [XmlElement("address")]
        public string Name { get; set; }

        [XmlElement("name")]
        public string Address { get; set; }
    }
}