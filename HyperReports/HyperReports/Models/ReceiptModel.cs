﻿using HyperReports.Attributes;
using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;
using static HyperReports.Shared.PaymentMethods;

namespace HyperReports.Models
{
    public class ReceiptModel : BaseModel
    {
        [XmlElement("total")]
        public decimal Total { get; set; }

        [XmlElement("datetime")]
        public DateTime ReceiptDateTime { get; set; }

        [XmlElement("payment")]
        public Payment Payment { get; set; }

        public int CardDetailsId { get; set; }

        [DoNotPopulate]
        [XmlElement("carddetails")]
        public CardDetailsModel CardDetails { get; set; }

        public int StoreId { get; set; }

        [DoNotPopulate]
        public StoreModel Store { get; set; }
    }
}