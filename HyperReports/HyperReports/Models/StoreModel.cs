﻿using HyperReports.Attributes;
using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;

namespace HyperReports.Models
{
    public class StoreModel : BaseModel
    {
        [XmlAttribute("name")]
        public string Name { get; set; }

        [XmlAttribute("address")]
        public string Address { get; set; }

        [DoNotPopulate]
        [XmlArray("receipts"), XmlArrayItem("receipt")]
        public ReceiptModel[] Receipts { get; set; }

        [DoNotPopulate]
        [XmlArray("invoices"), XmlArrayItem("invoice")]
        public InvoiceModel[] Invoices { get; set; }

        public int CompanyId { get; set; }

        [DoNotPopulate]
        public CompanyModel Company { get; set; }
    }
}