﻿using HyperReports.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace HyperReports.Models
{
    public abstract class BaseModel
    {
        [DoNotInsert]
        public int Id { get; set; }
    }
}