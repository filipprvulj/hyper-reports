﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;

namespace HyperReports.Models
{
    public class CardDetailsModel : BaseModel
    {
        [XmlElement("cardtype")]
        public string CardType { get; set; }

        [XmlElement("number")]
        public string CardNumber { get; set; }

        [XmlElement("contactless")]
        public bool IsContactless { get; set; }
    }
}