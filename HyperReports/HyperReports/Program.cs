﻿using Commander.NET;
using Commander.NET.Exceptions;
using HyperReports.CLI;
using HyperReports.DTOs;
using HyperReports.Entitites;
using HyperReports.Models;
using HyperReports.Repository;
using HyperReports.Services;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using static HyperReports.Shared.PaymentMethods;

namespace HyperReports
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            string[] arguments = { "process" };

            try
            {
                Options options = new CommanderParser<Options>().Parse(args);
            }
            catch (ParameterMissingException ex)
            {
                Console.WriteLine("Missing parameter: " + ex.ParameterName);
            }
            catch (ParameterFormatException ex)
            {
                Console.WriteLine(ex.Message);
            }
            catch (ParameterMatchException ex)
            {
                Console.WriteLine(ex.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                if (ex.InnerException != null)
                {
                    Console.WriteLine(ex.InnerException.Message);
                }
            }

            //Console.ReadLine();
        }
    }
}