﻿//using Renci.SshNet;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using WinSCP;

namespace HyperReports
{
    public class XmlFileSync
    {
        public void SyncFiles()
        {
            string remoteDirectory = @"/xml-data/";
            string localDirectory = Settings.XmlFiles.FilePath;

            Directory.CreateDirectory(localDirectory);

            try
            {
                SessionOptions sessionOptions = new SessionOptions
                {
                    Protocol = Protocol.Sftp,
                    HostName = Settings.SftpConfig.Host,
                    UserName = Settings.SftpConfig.Username,
                    Password = Settings.SftpConfig.Password,
                    PortNumber = Settings.SftpConfig.Port,
                    SshHostKeyFingerprint = Settings.SftpConfig.HostFingerprint
                };

                using (Session session = new Session())
                {
                    session.Open(sessionOptions);

                    SynchronizationResult synchronizationResult =
                        session.SynchronizeDirectories(SynchronizationMode.Local, localDirectory, remoteDirectory, false);

                    synchronizationResult.Check();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
    }
}