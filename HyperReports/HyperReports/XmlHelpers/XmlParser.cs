﻿using HyperReports.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace HyperReports
{
    public class XmlParser
    {
        public CompanyModel DeserializeFile(string filePath)
        {
            XmlSerializer xmlSerializer = new XmlSerializer(typeof(CompanyModel));
            CompanyModel company = null;

            using (Stream reader = new FileStream(filePath, FileMode.Open))
            {
                company = (CompanyModel)xmlSerializer.Deserialize(reader);
            }

            return company;
        }
    }
}