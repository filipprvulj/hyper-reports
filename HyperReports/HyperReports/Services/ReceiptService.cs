﻿using HyperReports.DTOs;
using HyperReports.Repository;
using System;
using System.Collections.Generic;
using System.Text;

namespace HyperReports.Services
{
    public class ReceiptService : BaseService<ReceiptDto, ReceiptRepository>
    {
        private readonly CardDetailsService _cardDetailsService;

        public ReceiptService(ReceiptRepository repository, CardDetailsService cardDetailsService) : base(repository)
        {
            _cardDetailsService = cardDetailsService;
        }

        public IEnumerable<ReceiptDto> GetReceiptsByStoreId(int storeId)
        {
            if (storeId == 0)
            {
                throw new ArgumentException("Store id must be a positive value");
            }

            IEnumerable<ReceiptDto> receipts = _repository.GetReceiptsByStore(storeId);
            return receipts;
        }

        public IEnumerable<ReceiptDto> GetReceiptsByCardId(int cardId)
        {
            if (cardId == 0)
            {
                throw new ArgumentException("Card id must be a positive value");
            }

            IEnumerable<ReceiptDto> receipts = _repository.GetReceiptsByCardId(cardId);
            return receipts;
        }

        public override int Insert(ReceiptDto entity, string identifier)
        {
            if (entity is null)
            {
                throw new ArgumentException("Receipt must have a value");
            }

            entity.CardDetailsId = null;
            if (entity.CardDetails != null)
            {
                entity.CardDetailsId = _cardDetailsService.Insert(entity.CardDetails, "CardNumber");
            }

            return _repository.Insert(entity);
        }
    }
}