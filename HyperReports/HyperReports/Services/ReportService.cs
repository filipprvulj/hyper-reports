﻿using ClosedXML.Excel;
using HyperReports.Repository;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace HyperReports.Services
{
    internal class ReportService
    {
        private readonly ReportRepository _repository;

        public ReportService(ReportRepository repository)
        {
            _repository = repository;
        }

        public void ExportMonthlyReportByStore(string companyName, int year, int month, string orderBy, int topN)
        {
            DataTable report = _repository.GetMonthlyTurnoverByStore(companyName, year, month, orderBy, topN);

            string destinationFolder = Settings.XmlFiles.ExportPath;
            string fileName = $"{destinationFolder}\\{companyName}_{month}_{year}_S.xlsx";

            SaveTable(report, fileName);
        }

        public void ExportQuarterlyReportByStore(string companyName, string quarter, int year, string orderBy, int topN)
        {
            DataTable report = _repository.GetQuarterlyTurnoverByStore(companyName, quarter, year, orderBy, topN);

            string destinationFolder = Settings.XmlFiles.ExportPath;
            string fileName = $"{destinationFolder}\\{companyName}_{quarter}_{year}_S.xlsx";

            SaveTable(report, fileName);
        }

        public void ExportYearlyReportByStore(string companyName, int year, string orderBy, int topN)
        {
            DataTable report = _repository.GetYearlyTurnoverByStore(companyName, year, orderBy, topN);

            string destinationFolder = Settings.XmlFiles.ExportPath;
            string fileName = $"{destinationFolder}\\{companyName}_{year}_S.xlsx";

            SaveTable(report, fileName);
        }

        public void ExportMonthlyReportByPayment(string companyName, int year, int month, string orderBy, int topN)
        {
            DataTable report = _repository.GetMonthlyTurnoverByPayment(companyName, year, month, orderBy, topN);

            string destinationFolder = Settings.XmlFiles.ExportPath;
            string fileName = $"{destinationFolder}\\{companyName}_{month}_{year}_P.xlsx";

            SaveTable(report, fileName);
        }

        public void ExportQuarterlyReportByPayment(string companyName, string quarter, int year, string orderBy, int topN)
        {
            DataTable report = _repository.GetQuarterlyTurnoverByPayment(companyName, quarter, year, orderBy, topN);

            string destinationFolder = Settings.XmlFiles.ExportPath;
            string fileName = $"{destinationFolder}\\{companyName}_{quarter}_{year}_P.xlsx";

            SaveTable(report, fileName);
        }

        public void ExportYearlyReportByPayment(string companyName, int year, string orderBy, int topN)
        {
            DataTable report = _repository.GetYearlyTurnoverByPayment(companyName, year, orderBy, topN);

            string destinationFolder = Settings.XmlFiles.ExportPath;
            string fileName = $"{destinationFolder}\\{companyName}_{year}_P.xlsx";

            SaveTable(report, fileName);
        }

        private void SaveTable(DataTable table, string fileName)
        {
            XLWorkbook wb = new XLWorkbook();
            var ws = wb.Worksheets.Add(table);
            ws.Row(1).Style.Fill.BackgroundColor = XLColor.AppleGreen;

            wb.SaveAs(fileName);
        }
    }
}