﻿using HyperReports.DTOs;
using HyperReports.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace HyperReports.Services
{
    public abstract class BaseService<TDto, TRepository>
        where TDto : BaseDto
        where TRepository : IBaseRepository<TDto>
    {
        protected readonly TRepository _repository;

        protected BaseService(TRepository repository)
        {
            _repository = repository;
        }

        public IEnumerable<TDto> GetAll()
        {
            var entities = _repository.GetAll();

            return entities;
        }

        public virtual TDto GetById(int id)
        {
            if (id == default)
            {
                throw new ArgumentException("The id must be positive number");
            }

            var entity = _repository.GetById(id);

            if (entity == null)
            {
                throw new ArgumentException("Entity not found");
            }

            return entity;
        }

        public virtual int Insert(TDto entity, string identifier)
        {
            if (entity == null)
            {
                throw new ArgumentNullException("entity", "Argument must have a value");
            }

            int id = default;
            if (typeof(TDto) != typeof(ReceiptDto) && typeof(TDto) != typeof(InvoiceDto))
            {
                id = _repository.GetDbEntityId(entity, identifier);
            }

            if (id == default)
            {
                id = _repository.Insert(entity);
            }

            return id;
        }
    }
}