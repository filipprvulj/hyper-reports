﻿using HyperReports.DTOs;
using HyperReports.Entitites;
using HyperReports.Repository;
using HyperReports.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace HyperReports.Services
{
    public class XmlFileService : IXmlFileService
    {
        private readonly XmlFileRepository _repository;

        public XmlFileService(XmlFileRepository repository)
        {
            _repository = repository;
        }

        public IEnumerable<CompanyDto> GetFilesData()
        {
            var companies = _repository.GetFilesData();
            if (!companies.Any())
            {
                Console.WriteLine("Directory is empty");
                return Enumerable.Empty<CompanyDto>();
            }

            return companies;
        }

        public CompanyDto GetFileData(string fileName)
        {
            var company = _repository.GetFileData(fileName);
            if (company == null)
            {
                Console.WriteLine("File does not exist");
            }
            return company;
        }

        public IEnumerable<CompanyDto> GetNewFilesData()
        {
            var companies = _repository.GetNewFilesData();
            if (!companies.Any())
            {
                Console.WriteLine("There is no new files");
                return Enumerable.Empty<CompanyDto>();
            }

            return companies;
        }
    }
}