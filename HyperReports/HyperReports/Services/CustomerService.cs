﻿using HyperReports.DTOs;
using HyperReports.Repository;
using System;
using System.Collections.Generic;
using System.Text;

namespace HyperReports.Services
{
    public class CustomerService : BaseService<CustomerDto, CustomerRepository>
    {
        public CustomerService(CustomerRepository repository) : base(repository)
        {
        }

        public CustomerDto GetCustomerByUuid(string uuid)
        {
            return _repository.GetCustomerByUuid(uuid);
        }
    }
}