﻿using HyperReports.DTOs;
using HyperReports.Repository;
using System;
using System.Collections.Generic;
using System.Text;

namespace HyperReports.Services
{
    public class CardDetailsService : BaseService<CardDetailsDto, CardDetailsRepository>
    {
        public CardDetailsService(CardDetailsRepository repository) : base(repository)
        {
        }

        public CardDetailsDto GetCardDetailsByCardNumber(string cardNumber)
        {
            return _repository.GetCardDetailsByCardNumber(cardNumber);
        }
    }
}