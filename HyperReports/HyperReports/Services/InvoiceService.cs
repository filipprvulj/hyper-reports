﻿using HyperReports.DTOs;
using HyperReports.Repository;
using System;
using System.Collections.Generic;
using System.Text;

namespace HyperReports.Services
{
    public class InvoiceService : BaseService<InvoiceDto, InvoiceRepository>
    {
        private readonly CardDetailsService _cardDetailsService;
        private readonly CustomerService _customerService;

        public InvoiceService(InvoiceRepository repository, CardDetailsService cardDetailsService, CustomerService customerService) : base(repository)
        {
            _cardDetailsService = cardDetailsService;
            _customerService = customerService;
        }

        public IEnumerable<InvoiceDto> GetInvoicesByStoreId(int storeId)
        {
            if (storeId == 0)
            {
                throw new ArgumentException("Store id must be a positive value");
            }

            IEnumerable<InvoiceDto> invoices = _repository.GetInvoicesByStore(storeId);
            return invoices;
        }

        public IEnumerable<InvoiceDto> GetInvoicesByCustomerId(int customerId)
        {
            if (customerId == 0)
            {
                throw new ArgumentException("Customer id must be a positive value");
            }

            IEnumerable<InvoiceDto> invoices = _repository.GetInvoicesByCustomer(customerId);
            return invoices;
        }

        public IEnumerable<InvoiceDto> GetInvoicesByCardId(int cardId)
        {
            if (cardId == 0)
            {
                throw new ArgumentException("Card id must be a positive value");
            }

            IEnumerable<InvoiceDto> invoices = _repository.GetInvoicesByCardId(cardId);
            return invoices;
        }

        public override int Insert(InvoiceDto entity, string identifier)
        {
            if (entity is null)
            {
                throw new ArgumentException("Invoice must have a value");
            }

            entity.CardDetailsId = null;
            if (entity.CardDetails != null)
            {
                entity.CardDetailsId = _cardDetailsService.Insert(entity.CardDetails, "CardNumber");
            }

            if (entity.Customer != null)
            {
                entity.CustomerId = _customerService.Insert(entity.Customer, "Uuid");
            }

            return _repository.Insert(entity);
        }
    }
}