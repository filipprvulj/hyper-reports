﻿using HyperReports.DTOs;
using HyperReports.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HyperReports.Services
{
    public class CompanyService : BaseService<CompanyDto, CompanyRepository>
    {
        private readonly StoreService _storeService;

        public CompanyService(CompanyRepository repository, StoreService storeService) : base(repository)
        {
            _storeService = storeService;
        }

        public override CompanyDto GetById(int id)
        {
            CompanyDto company = base.GetById(id);

            return company;
        }

        public CompanyDto GetCompanyByUuid(string uuid)
        {
            return _repository.GetCompanyByUuid(uuid);
        }

        public override int Insert(CompanyDto company, string identifier)
        {
            int companyId = base.Insert(company, identifier);
            foreach (StoreDto store in company.Stores)
            {
                store.CompanyId = companyId;
                _storeService.Insert(store, "Name");
            }

            return companyId;
        }
    }
}