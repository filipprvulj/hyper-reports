﻿using HyperReports.DTOs;
using HyperReports.Entitites;
using HyperReports.Models;
using HyperReports.Repository;
using System;
using System.Collections.Generic;
using System.Text;

namespace HyperReports.Services.Interfaces
{
    public interface IXmlFileService
    {
        public CompanyDto GetFileData(string fileName);

        public IEnumerable<CompanyDto> GetFilesData();
    }
}