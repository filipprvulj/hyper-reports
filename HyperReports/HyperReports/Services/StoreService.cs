﻿using HyperReports.DTOs;
using HyperReports.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HyperReports.Services
{
    public class StoreService : BaseService<StoreDto, StoreRepository>
    {
        private readonly ReceiptService _receiptService;
        private readonly InvoiceService _invoiceService;

        public StoreService(StoreRepository repository, ReceiptService receiptService, InvoiceService invoiceService) : base(repository)
        {
            _receiptService = receiptService;
            _invoiceService = invoiceService;
        }

        public IEnumerable<StoreDto> GetStoresForCompany(int companyId)
        {
            if (companyId == 0)
            {
                throw new ArgumentException("Company id must be a positive value");
            }

            IEnumerable<StoreDto> stores = _repository.GetStoresByCompanyId(companyId);
            return stores;
        }

        public override StoreDto GetById(int id)
        {
            StoreDto store = base.GetById(id);

            return store;
        }

        public StoreDto GetStoreByName(string name)
        {
            return _repository.GetStoreByName(name);
        }

        public override int Insert(StoreDto entity, string identifier)
        {
            int storeId = base.Insert(entity, identifier);
            if (storeId == default)
            {
                storeId = _repository.Insert(entity);
            }

            foreach (ReceiptDto receipt in entity.Receipts)
            {
                receipt.StoreId = storeId;
                _receiptService.Insert(receipt, null);
            }

            foreach (InvoiceDto invoice in entity.Invoices)
            {
                invoice.StoreId = storeId;
                _invoiceService.Insert(invoice, null);
            }

            return storeId;
        }
    }
}