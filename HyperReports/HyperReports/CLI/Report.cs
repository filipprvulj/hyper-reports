﻿using Commander.NET.Attributes;
using Commander.NET.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace HyperReports.CLI
{
    internal class Report : ICommand
    {
        [Parameter("--company", Required = Required.Yes)]
        public string company;

        [Parameter("--month", Required = Required.No, Regex = "^01|02|03|04|05|06|07|08|09|10|11|12$")]
        public string month;

        [Parameter("--quarter", Required = Required.No, Regex = "^q1|q2|q3|q4$")]
        public string quarter;

        [Parameter("--year", Required = Required.Yes)]
        public int year;

        [Parameter("--aggregation", Required = Required.Yes, Regex = "^store|receipt|invoice|payment$")]
        public string aggregation;

        [Parameter("--top", Required = Required.No)]
        public int topN;

        [Parameter("--order", Required = Required.No, Regex = "^asc|desc$")]
        public string order;

        void ICommand.Execute(object parent)
        {
        }
    }
}