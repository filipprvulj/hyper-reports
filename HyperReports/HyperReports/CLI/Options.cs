﻿using Commander.NET.Attributes;
using HyperReports.DTOs;
using HyperReports.Entitites;
using HyperReports.Repository;
using HyperReports.Services;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace HyperReports.CLI
{
    internal class Options
    {
        [Command("process")]
        public Process Process;

        [Command("config")]
        public Config Config;

        [Command("report")]
        public Report Report;

        [CommandHandler]
        public void ProcessCommand(Process process)
        {
            string connection = Settings.ConnectionString.DefaultConnection;

            CompanyRepository companyRepository = new CompanyRepository(connection);
            StoreRepository storeRepository = new StoreRepository(connection);
            CardDetailsRepository cardDetailsRepository = new CardDetailsRepository(connection);
            CustomerRepository customerRepository = new CustomerRepository(connection);
            InvoiceRepository invoiceRepository = new InvoiceRepository(connection);
            ReceiptRepository receiptRepository = new ReceiptRepository(connection);

            CardDetailsService cardDetailsService = new CardDetailsService(cardDetailsRepository);
            CustomerService customerService = new CustomerService(customerRepository);
            InvoiceService invoiceService = new InvoiceService(invoiceRepository, cardDetailsService, customerService);
            ReceiptService receiptService = new ReceiptService(receiptRepository, cardDetailsService);
            StoreService storeService = new StoreService(storeRepository, receiptService, invoiceService);
            CompanyService companyService = new CompanyService(companyRepository, storeService);

            XmlParser xmlParser = new XmlParser();
            XmlFileSync xmlFileSync = new XmlFileSync();

            XmlFileRepository repository = new XmlFileRepository(xmlFileSync, xmlParser);
            XmlFileService xmlFileService = new XmlFileService(repository);

            List<CompanyDto> companies = xmlFileService.GetNewFilesData().ToList();

            foreach (var company in companies)
            {
                companyService.Insert(company, "Uuid");
            }
        }

        [CommandHandler]
        public void ConfigCommand(Config config)
        {
            if (!string.IsNullOrEmpty(config.dataDir))
            {
                if (!Directory.Exists(config.dataDir))
                {
                    Console.WriteLine("Directory not found");
                }
                else
                {
                    Settings.SetFileDestination(config.dataDir);
                }
            }

            if (!string.IsNullOrEmpty(config.exportDir))
            {
                Directory.CreateDirectory(config.exportDir);
                Settings.SetExportPath(config.exportDir);
                Console.WriteLine("Directory created");
            }
        }

        [CommandHandler]
        public void ReportCommand(Report report)
        {
            string connection = Settings.ConnectionString.DefaultConnection;
            CompanyRepository companyRepository = new CompanyRepository(connection);
            ReportRepository reportRepository = new ReportRepository(connection, companyRepository);
            ReportService reportService = new ReportService(reportRepository);

            const string exportSuccessMessage = "Export successful";
            string orderBy = "asc";
            if (!string.IsNullOrEmpty(report.order))
            {
                orderBy = report.order;
            }

            if (!string.IsNullOrEmpty(report.quarter))
            {
                if (report.aggregation.Equals("store"))
                {
                    reportService.ExportQuarterlyReportByStore(report.company, report.quarter, report.year, orderBy, report.topN);
                    Console.WriteLine(exportSuccessMessage);
                }

                if (report.aggregation.Equals("payment"))
                {
                    reportService.ExportQuarterlyReportByPayment(report.company, report.quarter, report.year, orderBy, report.topN);
                    Console.WriteLine(exportSuccessMessage);
                }
            }

            if (!string.IsNullOrEmpty(report.month))
            {
                int month = Int32.Parse(report.month);

                if (report.aggregation.Equals("store"))
                {
                    reportService.ExportMonthlyReportByStore(report.company, report.year, month, orderBy, report.topN);
                    Console.WriteLine(exportSuccessMessage);
                }

                if (report.aggregation.Equals("payment"))
                {
                    reportService.ExportMonthlyReportByPayment(report.company, report.year, month, orderBy, report.topN);
                    Console.WriteLine(exportSuccessMessage);
                }
            }

            if (string.IsNullOrEmpty(report.quarter) && string.IsNullOrEmpty(report.month))
            {
                if (report.aggregation.Equals("store"))
                {
                    reportService.ExportYearlyReportByStore(report.company, report.year, orderBy, report.topN);
                    Console.WriteLine(exportSuccessMessage);
                }

                if (report.aggregation.Equals("payment"))
                {
                    reportService.ExportYearlyReportByPayment(report.company, report.year, orderBy, report.topN);
                    Console.WriteLine(exportSuccessMessage);
                }
            }
        }
    }
}