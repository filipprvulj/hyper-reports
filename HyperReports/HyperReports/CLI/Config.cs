﻿using Commander.NET.Attributes;
using Commander.NET.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace HyperReports.CLI
{
    internal class Config : ICommand
    {
        [Parameter("--data-dir", Required = Required.No)]
        public string dataDir;

        [Parameter("--export-dir", Required = Required.No)]
        public string exportDir;

        void ICommand.Execute(object parent)
        {
        }
    }
}