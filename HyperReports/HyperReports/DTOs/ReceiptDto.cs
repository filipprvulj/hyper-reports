﻿using System;
using System.Collections.Generic;
using System.Text;
using static HyperReports.Shared.PaymentMethods;

namespace HyperReports.DTOs
{
    public class ReceiptDto : BaseDto
    {
        public decimal Total { get; set; }

        public DateTime ReceiptDateTime { get; set; }

        public Payment Payment { get; set; }

        public int? CardDetailsId { get; set; }

        public CardDetailsDto CardDetails { get; set; }

        public int StoreId { get; set; }

        public StoreDto Store { get; set; }
    }
}