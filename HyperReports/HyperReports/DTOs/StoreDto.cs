﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HyperReports.DTOs
{
    public class StoreDto : BaseDto
    {
        public string Name { get; set; }

        public string Address { get; set; }

        public ReceiptDto[] Receipts { get; set; }

        public InvoiceDto[] Invoices { get; set; }

        public int CompanyId { get; set; }

        public CompanyDto Company { get; set; }
    }
}