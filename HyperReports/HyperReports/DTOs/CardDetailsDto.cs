﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HyperReports.DTOs
{
    public class CardDetailsDto : BaseDto
    {
        public string CardType { get; set; }

        public string CardNumber { get; set; }

        public bool IsContactless { get; set; }
    }
}