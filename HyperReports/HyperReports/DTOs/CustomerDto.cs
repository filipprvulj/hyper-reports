﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HyperReports.DTOs
{
    public class CustomerDto : BaseDto
    {
        public string Uuid { get; set; }

        public string Name { get; set; }

        public string Address { get; set; }
    }
}