﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HyperReports.DTOs
{
    public class CompanyDto : BaseDto
    {
        public string Uuid { get; set; }

        public string CompanyName { get; set; }

        public string Address { get; set; }

        public StoreDto[] Stores { get; set; }
    }
}