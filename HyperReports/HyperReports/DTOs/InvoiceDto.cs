﻿using System;
using System.Collections.Generic;
using System.Text;
using static HyperReports.Shared.PaymentMethods;

namespace HyperReports.DTOs
{
    public class InvoiceDto : BaseDto
    {
        public decimal Total { get; set; }

        public DateTime InvoiceDateTime { get; set; }

        public int CustomerId { get; set; }

        public CustomerDto Customer { get; set; }

        public Payment Payment { get; set; }

        public int? CardDetailsId { get; set; }

        public CardDetailsDto CardDetails { get; set; }

        public int StoreId { get; set; }

        public StoreDto Store { get; set; }
    }
}