﻿using AutoMapper;
using HyperReports.DTOs;
using HyperReports.Entitites;
using HyperReports.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace HyperReports.AutoMapper
{
    public static class AutoMapperConfig
    {
        public static MapperConfiguration Configuration()
        {
            var config = new MapperConfiguration(
                cfg =>
                {
                    cfg.CreateMap<CompanyModel, CompanyDto>();
                    cfg.CreateMap<StoreModel, StoreDto>();
                    cfg.CreateMap<CardDetailsModel, CardDetailsDto>();
                    cfg.CreateMap<CustomerModel, CustomerDto>();
                    cfg.CreateMap<InvoiceModel, InvoiceDto>();
                    cfg.CreateMap<ReceiptModel, ReceiptDto>();

                    cfg.CreateMap<CompanyEntity, CompanyDto>();
                    cfg.CreateMap<StoreEntity, StoreDto>();
                    cfg.CreateMap<CardDetailsEntity, CardDetailsDto>();
                    cfg.CreateMap<CustomerEntity, CustomerDto>();
                    cfg.CreateMap<InvoiceEntity, InvoiceDto>();
                    cfg.CreateMap<ReceiptEntity, ReceiptDto>();

                    cfg.CreateMap<CompanyDto, CompanyEntity>();
                    cfg.CreateMap<StoreDto, StoreEntity>();
                    cfg.CreateMap<CardDetailsDto, CardDetailsEntity>();
                    cfg.CreateMap<CustomerDto, CustomerEntity>();
                    cfg.CreateMap<InvoiceDto, InvoiceEntity>();
                    cfg.CreateMap<ReceiptDto, ReceiptEntity>();
                }
           );

            config.AssertConfigurationIsValid();
            return config;
        }
    }
}