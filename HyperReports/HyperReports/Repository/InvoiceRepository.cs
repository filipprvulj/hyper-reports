﻿using HyperReports.DTOs;
using HyperReports.Entitites;
using HyperReports.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;

namespace HyperReports.Repository
{
    public class InvoiceRepository : BaseRepository<InvoiceDto, InvoiceEntity>, IInvoiceRepository
    {
        public InvoiceRepository(string connection) : base(connection)
        {
        }

        public IEnumerable<InvoiceDto> GetInvoicesByCardId(int cardDetailsId)
        {
            if (cardDetailsId == default)
            {
                throw new ArgumentNullException("cardDetailsId", "Card id must be provided");
            }

            using SqlConnection connection = new SqlConnection(_connection);
            string query = "SELECT * FROM Invoice WHERE CardDetailsId = @CardDetialsId";

            SqlCommand command = connection.CreateCommand();
            command.CommandText = query;
            command.Parameters.AddWithValue("CardDetailId", cardDetailsId);

            List<InvoiceDto> invoices = new List<InvoiceDto>();
            connection.Open();
            using (var reader = command.ExecuteReader())
            {
                while (reader.Read())
                {
                    InvoiceEntity invoice = new InvoiceEntity();
                    PopulateModel(invoice, reader);
                    var invoiceDto = _mapper.Map<InvoiceDto>(invoice);
                    invoices.Add(invoiceDto);
                }
            }
            connection.Close();
            return invoices;
        }

        public IEnumerable<InvoiceDto> GetInvoicesByCustomer(int customerId)
        {
            if (customerId == default)
            {
                throw new ArgumentNullException("customerId", "Customer id must be provided");
            }

            using SqlConnection connection = new SqlConnection(_connection);
            string query = "SELECT * FROM Invoice WHERE CustomerId = @CustomerId";

            SqlCommand command = connection.CreateCommand();
            command.CommandText = query;
            command.Parameters.AddWithValue("CustomerId", customerId);

            List<InvoiceDto> invoices = new List<InvoiceDto>();
            connection.Open();
            using (var reader = command.ExecuteReader())
            {
                while (reader.Read())
                {
                    InvoiceEntity invoice = new InvoiceEntity();
                    PopulateModel(invoice, reader);
                    var invoiceDto = _mapper.Map<InvoiceDto>(invoice);
                    invoices.Add(invoiceDto);
                }
            }
            connection.Close();
            return invoices;
        }

        public IEnumerable<InvoiceDto> GetInvoicesByStore(int storeId)
        {
            if (storeId == default)
            {
                throw new ArgumentNullException("storeId", "Store id must be provided");
            }

            using SqlConnection connection = new SqlConnection(_connection);
            string query = "SELECT * FROM Invoice WHERE StoreId = @StoreId";

            SqlCommand command = connection.CreateCommand();
            command.CommandText = query;
            command.Parameters.AddWithValue("StoreId", storeId);

            List<InvoiceDto> invoices = new List<InvoiceDto>();
            connection.Open();
            using (var reader = command.ExecuteReader())
            {
                while (reader.Read())
                {
                    InvoiceEntity invoice = new InvoiceEntity();
                    PopulateModel(invoice, reader);
                    var invoiceDto = _mapper.Map<InvoiceDto>(invoice);
                    invoices.Add(invoiceDto);
                }
            }
            connection.Close();
            return invoices;
        }
    }
}