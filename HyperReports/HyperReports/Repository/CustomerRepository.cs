﻿using HyperReports.DTOs;
using HyperReports.Entitites;
using HyperReports.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;

namespace HyperReports.Repository
{
    public class CustomerRepository : BaseRepository<CustomerDto, CustomerEntity>, ICustomerRepository
    {
        public CustomerRepository(string connection) : base(connection)
        {
        }

        public CustomerDto GetCustomerByUuid(string uuid)
        {
            if (string.IsNullOrEmpty(uuid))
            {
                throw new ArgumentException("You must provide uuid");
            }

            SqlConnection connection = new SqlConnection(_connection);
            string query = "SELECT * FROM Customer WHERE uuid = @uuid";

            SqlCommand command = connection.CreateCommand();
            command.CommandText = query;
            command.Parameters.AddWithValue("uuid", uuid);

            connection.Open();
            using (var reader = command.ExecuteReader())
            {
                if (!reader.Read())
                {
                    connection.Close();
                    return default;
                }

                CustomerEntity customer = new CustomerEntity();
                PopulateModel(customer, reader);
                var customerDto = _mapper.Map<CustomerDto>(customer);
                connection.Close();
                return customerDto;
            }
        }
    }
}