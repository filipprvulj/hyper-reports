﻿using HyperReports.DTOs;
using HyperReports.Entitites;
using HyperReports.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;

namespace HyperReports.Repository
{
    public class ReceiptRepository : BaseRepository<ReceiptDto, ReceiptEntity>, IReceiptRepository
    {
        public ReceiptRepository(string connection) : base(connection)
        {
        }

        public IEnumerable<DTOs.ReceiptDto> GetReceiptsByCardId(int cardDetailsId)
        {
            if (cardDetailsId == default)
            {
                throw new ArgumentNullException("cardDetailsId", "Card id must be provided");
            }

            using SqlConnection connection = new SqlConnection(_connection);
            string query = "SELECT * FROM Receipt WHERE cardDetailsId = @CardDetailsId";

            SqlCommand command = connection.CreateCommand();
            command.CommandText = query;
            command.Parameters.AddWithValue("CardDetailsId", cardDetailsId);

            List<DTOs.ReceiptDto> receipts = new List<DTOs.ReceiptDto>();
            connection.Open();
            using (var reader = command.ExecuteReader())
            {
                while (reader.Read())
                {
                    ReceiptEntity receipt = new ReceiptEntity();
                    PopulateModel(receipt, reader);
                    var receiptDto = _mapper.Map<DTOs.ReceiptDto>(receipt);
                    receipts.Add(receiptDto);
                }
            }
            connection.Close();

            return receipts;
        }

        public IEnumerable<DTOs.ReceiptDto> GetReceiptsByStore(int storeId)
        {
            if (storeId == default)
            {
                throw new ArgumentNullException("storeId", "Store id must be provided");
            }

            using SqlConnection connection = new SqlConnection(_connection);
            string query = "SELECT * FROM Receipt WHERE StoreId = @StoreId";

            SqlCommand command = new SqlCommand();
            command.CommandText = query;
            command.Parameters.AddWithValue("StoreId", storeId);

            List<DTOs.ReceiptDto> receipts = new List<DTOs.ReceiptDto>();
            connection.Open();
            using (var reader = command.ExecuteReader())
            {
                while (reader.Read())
                {
                    ReceiptEntity receipt = new ReceiptEntity();
                    PopulateModel(receipt, reader);
                    var receiptDto = _mapper.Map<ReceiptDto>(receipt);
                    receipts.Add(receiptDto);
                }
            }
            connection.Close();
            return receipts;
        }
    }
}