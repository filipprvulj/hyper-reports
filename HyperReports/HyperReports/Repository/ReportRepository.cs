﻿using HyperReports.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Text;

namespace HyperReports.Repository
{
    public class ReportRepository : IReportRepository
    {
        private readonly string _connection;
        private readonly CompanyRepository _companyRepository;

        public ReportRepository(string connection, CompanyRepository companyRepository)
        {
            _connection = connection;
            _companyRepository = companyRepository;
        }

        public DataTable GetQuarterlyTurnoverByStore(string companyName, string quarter, int year, string orderBy, int topN = 0)
        {
            StringBuilder query = new StringBuilder();
            var months = GetMonths(quarter);
            var companyId = GetCompanyId(companyName);
            string top;
            if (topN != 0)
            {
                top = $"TOP({topN})";
            }
            else
            {
                top = "";
            }

            query.Append($"SELECT {top} (SELECT Name From Store WHERE id = StoreId) as Store,");
            foreach (var month in months)
            {
                query.Append($"ROUND(SUM(CASE WHEN DATEPART(MONTH, Date) = {month} THEN Total END), 2) {CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(month)},");
            }
            query.Append("ROUND(SUM(Total), 2) AS Total ");
            query.Append(@"FROM(
                            SELECT Id, Total, ReceiptDateTime as Date, Payment, StoreId, CardDetailsId FROM Receipt
                            UNION ALL
                            SELECT Id, Total, InvoiceDateTime as Date, Payment, StoreId, CardDetailsId FROM Invoice
                          ) AS UA ");
            query.Append($"WHERE StoreId IN (SELECT id FROM Store WHERE CompanyId = {companyId}) ");
            query.Append($"AND Year(Date) = {year} ");
            query.Append("GROUP BY StoreID ");
            query.Append($"ORDER BY Store {orderBy}");

            SqlConnection connection = new SqlConnection(_connection);
            connection.Open();
            SqlCommand command = connection.CreateCommand();
            command.CommandText = query.ToString();

            DataTable report = new DataTable();
            report.Load(command.ExecuteReader());
            report.TableName = companyName;
            connection.Close();
            return report;
        }

        public DataTable GetYearlyTurnoverByStore(string companyName, int year, string orderBy, int topN = 0)
        {
            StringBuilder query = new StringBuilder();
            var companyId = GetCompanyId(companyName);
            string top;
            if (topN != 0)
            {
                top = $"TOP({topN})";
            }
            else
            {
                top = "";
            }

            query.Append($"SELECT {top} (SELECT Name From Store WHERE id = StoreId) as Store,");
            query.Append($"ROUND(SUM(CASE WHEN DATEPART(QUARTER, Date) = 1 THEN Total END), 2) q1,");
            query.Append($"ROUND(SUM(CASE WHEN DATEPART(QUARTER, Date) = 2 THEN Total END), 2) q2,");
            query.Append($"ROUND(SUM(CASE WHEN DATEPART(QUARTER, Date) = 3 THEN Total END), 2) q3,");
            query.Append($"ROUND(SUM(CASE WHEN DATEPART(QUARTER, Date) = 4 THEN Total END), 2) q4,");
            query.Append("ROUND(SUM(Total), 2) AS Total ");
            query.Append(@"FROM(
                            SELECT Id, Total, ReceiptDateTime as Date, Payment, StoreId, CardDetailsId FROM Receipt
                            UNION ALL
                            SELECT Id, Total, InvoiceDateTime as Date, Payment, StoreId, CardDetailsId FROM Invoice
                          ) AS UA ");
            query.Append($"WHERE StoreId IN (SELECT id FROM Store WHERE CompanyId = {companyId}) ");
            query.Append($"AND Year(Date) = {year}");
            query.Append("GROUP BY StoreID ");
            query.Append($"ORDER BY Store {orderBy}");

            SqlConnection connection = new SqlConnection(_connection);
            connection.Open();
            SqlCommand command = connection.CreateCommand();
            command.CommandText = query.ToString();

            DataTable report = new DataTable();
            report.Load(command.ExecuteReader());
            report.TableName = companyName;
            connection.Close();
            return report;
        }

        public DataTable GetMonthlyTurnoverByStore(string companyName, int year, int month, string orderBy, int topN = 0)
        {
            StringBuilder query = new StringBuilder();
            var days = GetDaysInMonth(year, month);
            var companyId = GetCompanyId(companyName);
            string top;
            if (topN != 0)
            {
                top = $"TOP({topN})";
            }
            else
            {
                top = "";
            }

            query.Append($"SELECT {top} (SELECT Name From Store WHERE id = StoreId) as Store,");
            foreach (var day in days)
            {
                query.Append($"ROUND(SUM(CASE WHEN DATEPART(Day, Date) = {day} THEN Total END), 2) as 'Day {day}',");
            }
            query.Append("ROUND(SUM(Total), 2) AS Total ");
            query.Append(@"FROM(
                            SELECT Id, Total, ReceiptDateTime as Date, Payment, StoreId, CardDetailsId FROM Receipt
                            UNION ALL
                            SELECT Id, Total, InvoiceDateTime as Date, Payment, StoreId, CardDetailsId FROM Invoice
                          ) AS UA ");
            query.Append($"WHERE StoreId IN (SELECT id FROM Store WHERE CompanyId = {companyId}) ");
            query.Append($"AND Year(Date) = {year} AND MONTH(Date) = {month}");
            query.Append("GROUP BY StoreID ");
            query.Append($"ORDER BY Store {orderBy}");

            SqlConnection connection = new SqlConnection(_connection);
            connection.Open();
            SqlCommand command = connection.CreateCommand();
            command.CommandText = query.ToString();

            DataTable report = new DataTable();
            report.Load(command.ExecuteReader());
            report.TableName = companyName;
            connection.Close();
            return report;
        }

        public DataTable GetQuarterlyTurnoverByPayment(string companyName, string quarter, int year, string orderBy, int topN = 0)
        {
            StringBuilder query = new StringBuilder();
            var months = GetMonths(quarter);
            var companyId = GetCompanyId(companyName);
            string top;
            if (topN != 0)
            {
                top = $"TOP({topN})";
            }
            else
            {
                top = "";
            }

            query.Append($"SELECT {top} (CASE Payment WHEN 0 THEN 'Cash' WHEN 1 THEN 'Card' END) AS Payment,");
            foreach (var month in months)
            {
                query.Append($"ROUND(SUM(CASE WHEN DATEPART(MONTH, Date) = {month} THEN Total END), 2) {CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(month)},");
            }
            query.Append("ROUND(SUM(Total), 2) AS Total ");
            query.Append(@"FROM(
                            SELECT Id, Total, ReceiptDateTime as Date, Payment, StoreId, CardDetailsId FROM Receipt
                            UNION ALL
                            SELECT Id, Total, InvoiceDateTime as Date, Payment, StoreId, CardDetailsId FROM Invoice
                          ) AS UA ");
            query.Append($"WHERE StoreId IN (SELECT id FROM Store WHERE CompanyId = {companyId}) ");
            query.Append($"AND Year(Date) = {year} ");
            query.Append("GROUP BY Payment ");
            query.Append($"ORDER BY Payment {orderBy}");

            SqlConnection connection = new SqlConnection(_connection);
            connection.Open();
            SqlCommand command = connection.CreateCommand();
            command.CommandText = query.ToString();

            DataTable report = new DataTable();
            report.Load(command.ExecuteReader());
            report.TableName = companyName;
            connection.Close();
            return report;
        }

        public DataTable GetYearlyTurnoverByPayment(string companyName, int year, string orderBy, int topN = 0)
        {
            StringBuilder query = new StringBuilder();
            var companyId = GetCompanyId(companyName);
            string top;
            if (topN != 0)
            {
                top = $"TOP({topN})";
            }
            else
            {
                top = "";
            }
            query.Append($"SELECT {top} (CASE Payment WHEN 0 THEN 'Cash' WHEN 1 THEN 'Card' END) AS Payment,");
            query.Append($"ROUND(SUM(CASE WHEN DATEPART(QUARTER, Date) = 1 THEN Total END), 2) q1,");
            query.Append($"ROUND(SUM(CASE WHEN DATEPART(QUARTER, Date) = 2 THEN Total END), 2) q2,");
            query.Append($"ROUND(SUM(CASE WHEN DATEPART(QUARTER, Date) = 3 THEN Total END), 2) q3,");
            query.Append($"ROUND(SUM(CASE WHEN DATEPART(QUARTER, Date) = 4 THEN Total END), 2) q4,");
            query.Append("ROUND(SUM(Total), 2) AS Total ");
            query.Append(@"FROM(
                            SELECT Id, Total, ReceiptDateTime as Date, Payment, StoreId, CardDetailsId FROM Receipt
                            UNION ALL
                            SELECT Id, Total, InvoiceDateTime as Date, Payment, StoreId, CardDetailsId FROM Invoice
                          ) AS UA ");
            query.Append($"WHERE StoreId IN (SELECT id FROM Store WHERE CompanyId = {companyId}) ");
            query.Append($"AND Year(Date) = {year}");
            query.Append("GROUP BY Payment ");
            query.Append($"ORDER BY Payment {orderBy}");

            SqlConnection connection = new SqlConnection(_connection);
            connection.Open();
            SqlCommand command = connection.CreateCommand();
            command.CommandText = query.ToString();

            DataTable report = new DataTable();
            report.Load(command.ExecuteReader());
            report.TableName = companyName;
            connection.Close();
            return report;
        }

        public DataTable GetMonthlyTurnoverByPayment(string companyName, int year, int month, string orderBy, int topN = 0)
        {
            StringBuilder query = new StringBuilder();
            var days = GetDaysInMonth(year, month);
            var companyId = GetCompanyId(companyName);
            string top;
            if (topN != 0)
            {
                top = $"TOP({topN})";
            }
            else
            {
                top = "";
            }

            query.Append($"SELECT {top} (CASE Payment WHEN 0 THEN 'Cash' WHEN 1 THEN 'Card' END) AS Payment,");
            foreach (var day in days)
            {
                query.Append($"ROUND(SUM(CASE WHEN DATEPART(Day, Date) = {day} THEN Total END), 2) as 'Day {day}',");
            }
            query.Append("ROUND(SUM(Total), 2) AS Total ");
            query.Append(@"FROM(
                            SELECT Id, Total, ReceiptDateTime as Date, Payment, StoreId, CardDetailsId FROM Receipt
                            UNION ALL
                            SELECT Id, Total, InvoiceDateTime as Date, Payment, StoreId, CardDetailsId FROM Invoice
                          ) AS UA ");
            query.Append($"WHERE StoreId IN (SELECT id FROM Store WHERE CompanyId = {companyId}) ");
            query.Append($"AND Year(Date) = {year} AND MONTH(Date) = {month}");
            query.Append("GROUP BY Payment ");
            query.Append($"ORDER BY Payment {orderBy}");

            SqlConnection connection = new SqlConnection(_connection);
            connection.Open();
            SqlCommand command = connection.CreateCommand();
            command.CommandText = query.ToString();

            DataTable report = new DataTable();
            report.Load(command.ExecuteReader());
            report.TableName = companyName;
            connection.Close();
            return report;
        }

        private int[] GetMonths(string quarter)
        {
            int[] months;
            switch (quarter)
            {
                case "q1":
                    months = new int[] { 1, 2, 3 };
                    break;

                case "q2":
                    months = new int[] { 4, 5, 6 };
                    break;

                case "q3":
                    months = new int[] { 7, 8, 9 };
                    break;

                case "q4":
                    months = new int[] { 10, 11, 12 };
                    break;

                default:
                    months = new int[0];
                    break;
            }

            return months;
        }

        private int[] GetDaysInMonth(int year, int month)
        {
            return Enumerable.Range(1, DateTime.DaysInMonth(year, month)).ToArray();
        }

        private int GetCompanyId(string companyName)
        {
            var company = _companyRepository.GetCompanyByName(companyName);
            if (company != null)
            {
                return company.Id;
            }
            else
            {
                return default;
            }
        }
    }
}