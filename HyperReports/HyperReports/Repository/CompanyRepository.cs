﻿using HyperReports.DTOs;
using HyperReports.Entitites;
using HyperReports.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;

namespace HyperReports.Repository
{
    public class CompanyRepository : BaseRepository<CompanyDto, CompanyEntity>, ICompanyRepository
    {
        public CompanyRepository(string connection) : base(connection)
        {
        }

        public CompanyDto GetCompanyByUuid(string uuid)
        {
            if (string.IsNullOrEmpty(uuid))
            {
                throw new ArgumentException("You must provide uuid");
            }

            SqlConnection connection = new SqlConnection(_connection);
            string query = "SELECT * FROM Company WHERE uuid = @uuid";
            SqlCommand command = connection.CreateCommand();
            command.CommandText = query;
            command.Parameters.AddWithValue("uuid", uuid);

            connection.Open();
            using (var reader = command.ExecuteReader())
            {
                if (!reader.Read())
                {
                    connection.Close();
                    return default;
                }
                CompanyEntity company = new CompanyEntity();
                PopulateModel(company, reader);
                var companyDto = _mapper.Map<CompanyDto>(company);
                connection.Close();
                return companyDto;
            }
        }

        public CompanyDto GetCompanyByName(string name)
        {
            if (string.IsNullOrEmpty(name))
            {
                throw new ArgumentException("You must provide a company name");
            }

            SqlConnection connection = new SqlConnection(_connection);
            string query = "SELECT * FROM Company WHERE CompanyName = @Name";
            SqlCommand command = connection.CreateCommand();
            command.CommandText = query;
            command.Parameters.AddWithValue("Name", name);

            connection.Open();
            using (var reader = command.ExecuteReader())
            {
                if (!reader.Read())
                {
                    connection.Close();
                    return default;
                }
                CompanyEntity company = new CompanyEntity();
                PopulateModel(company, reader);
                var companyDto = _mapper.Map<CompanyDto>(company);
                connection.Close();
                return companyDto;
            }
        }
    }
}