﻿using AutoMapper;
using HyperReports.AutoMapper;
using HyperReports.DTOs;
using HyperReports.Models;
using HyperReports.Repository.Interfaces;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace HyperReports.Repository
{
    public class XmlFileRepository : IXmlFileRepository
    {
        private readonly XmlParser _xmlParser;
        private readonly XmlFileSync _xmlFileSync;
        private readonly string _xmlFilesDirectory;
        private readonly IMapper _mapper;

        public XmlFileRepository(XmlFileSync xmlFileSync, XmlParser xmlParser)
        {
            _xmlFilesDirectory = Settings.XmlFiles.FilePath;
            _xmlParser = xmlParser;
            _xmlFileSync = xmlFileSync;
            _mapper = AutoMapperConfig.Configuration().CreateMapper();
        }

        public IEnumerable<CompanyDto> GetFilesData()
        {
            _xmlFileSync.SyncFiles();
            List<CompanyDto> companies = new List<CompanyDto>();
            FileInfo[] xmlFiles = new DirectoryInfo(_xmlFilesDirectory).GetFiles();

            foreach (var xmlFile in xmlFiles)
            {
                string filePath = Path.Combine(_xmlFilesDirectory, xmlFile.Name);
                CompanyModel companyModel = _xmlParser.DeserializeFile(filePath);
                CompanyDto companyDto = _mapper.Map<CompanyDto>(companyModel);
                companies.Add(companyDto);
            }

            Settings.SetLastFileProcessed(xmlFiles.Last().Name);

            return companies;
        }

        public IEnumerable<CompanyDto> GetNewFilesData()
        {
            List<FileInfo> newFiles = GetNewFiles();

            List<CompanyDto> companies = new List<CompanyDto>();
            foreach (var xmlFile in newFiles)
            {
                string filePath = Path.Combine(_xmlFilesDirectory, xmlFile.Name);
                CompanyModel companyModel = _xmlParser.DeserializeFile(filePath);
                CompanyDto companyDto = _mapper.Map<CompanyDto>(companyModel);

                companies.Add(companyDto);
            }

            var lastProcessedFile = newFiles.LastOrDefault();

            if (lastProcessedFile != null)
            {
                Settings.SetLastFileProcessed(lastProcessedFile.Name);
            }
            return companies;
        }

        public List<FileInfo> GetNewFiles()
        {
            _xmlFileSync.SyncFiles();

            List<FileInfo> files = new DirectoryInfo(_xmlFilesDirectory)
                .GetFiles()
                .ToList();

            files.Sort((a, b) => string.Compare(a.Name, b.Name));

            List<FileInfo> newFiles;
            if (string.IsNullOrEmpty(Settings.XmlFiles.LastFileProcessed))
            {
                newFiles = files;
            }
            else
            {
                int indexOfLastProcessed = files.FindIndex(f => f.Name == Settings.XmlFiles.LastFileProcessed);
                if (indexOfLastProcessed != -1 && files.Count > 0)
                {
                    int count = files.IndexOf(files.Last()) - indexOfLastProcessed;
                    newFiles = files.GetRange(indexOfLastProcessed + 1, count);
                }
                else
                {
                    newFiles = files;
                }
            }

            return newFiles;
        }

        public CompanyDto GetFileData(string fileName)
        {
            string filePath = Path.Combine(_xmlFilesDirectory, fileName);
            CompanyModel companyModel = _xmlParser.DeserializeFile(filePath);
            CompanyDto companyDto = _mapper.Map<CompanyDto>(companyModel);
            return companyDto;
        }
    }
}