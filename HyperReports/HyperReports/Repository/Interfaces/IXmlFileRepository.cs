﻿using HyperReports.DTOs;
using System;
using System.Collections.Generic;
using System.Text;

namespace HyperReports.Repository.Interfaces
{
    public interface IXmlFileRepository
    {
        public IEnumerable<CompanyDto> GetFilesData();

        public CompanyDto GetFileData(string fileName);
    }
}