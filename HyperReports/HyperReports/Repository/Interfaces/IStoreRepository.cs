﻿using HyperReports.DTOs;
using System;
using System.Collections.Generic;
using System.Text;

namespace HyperReports.Repository.Interfaces
{
    public interface IStoreRepository : IBaseRepository<StoreDto>
    {
        IEnumerable<StoreDto> GetStoresByCompanyId(int companyId);

        StoreDto GetStoreByName(string name);
    }
}