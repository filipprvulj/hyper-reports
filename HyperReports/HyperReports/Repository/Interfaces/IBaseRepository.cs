﻿using HyperReports.DTOs;
using HyperReports.Entitites;

using System;
using System.Collections.Generic;
using System.Text;

namespace HyperReports.Repository.Interfaces
{
    public interface IBaseRepository<TDto>
        where TDto : BaseDto
    {
        TDto GetById(int id);

        IEnumerable<TDto> GetAll();

        int Insert(TDto dto);

        public int GetDbEntityId(TDto dto, string identifier);
    }
}