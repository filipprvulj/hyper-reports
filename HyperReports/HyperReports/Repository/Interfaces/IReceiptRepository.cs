﻿using HyperReports.DTOs;
using System;
using System.Collections.Generic;
using System.Text;

namespace HyperReports.Repository.Interfaces
{
    public interface IReceiptRepository : IBaseRepository<ReceiptDto>
    {
        IEnumerable<ReceiptDto> GetReceiptsByStore(int storeId);

        IEnumerable<ReceiptDto> GetReceiptsByCardId(int cardDetailsId);
    }
}