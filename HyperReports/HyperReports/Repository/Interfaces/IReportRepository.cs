﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace HyperReports.Repository.Interfaces
{
    public interface IReportRepository
    {
        DataTable GetQuarterlyTurnoverByStore(string companyName, string quarter, int year, string orderBy, int topN = 0);

        DataTable GetYearlyTurnoverByStore(string companyName, int year, string orderBy, int topN = 0);

        DataTable GetMonthlyTurnoverByStore(string companyName, int year, int month, string orderBy, int topN = 0);

        DataTable GetQuarterlyTurnoverByPayment(string companyName, string quarter, int year, string orderBy, int topN = 0);

        DataTable GetYearlyTurnoverByPayment(string companyName, int year, string orderBy, int topN = 0);

        DataTable GetMonthlyTurnoverByPayment(string companyName, int year, int month, string orderBy, int topN = 0);
    }
}