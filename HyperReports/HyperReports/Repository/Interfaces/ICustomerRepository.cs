﻿using HyperReports.DTOs;
using System;
using System.Collections.Generic;
using System.Text;

namespace HyperReports.Repository.Interfaces
{
    public interface ICustomerRepository : IBaseRepository<CustomerDto>
    {
        CustomerDto GetCustomerByUuid(string uuid);
    }
}