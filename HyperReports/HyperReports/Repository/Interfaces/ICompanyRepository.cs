﻿using HyperReports.DTOs;

using System;
using System.Collections.Generic;
using System.Text;

namespace HyperReports.Repository.Interfaces
{
    public interface ICompanyRepository : IBaseRepository<CompanyDto>
    {
        CompanyDto GetCompanyByUuid(string uuid);
    }
}