﻿using HyperReports.DTOs;
using System;
using System.Collections.Generic;
using System.Text;

namespace HyperReports.Repository.Interfaces
{
    public interface ICardDetailsRepository : IBaseRepository<CardDetailsDto>
    {
        CardDetailsDto GetCardDetailsByCardNumber(string cardNumber);
    }
}