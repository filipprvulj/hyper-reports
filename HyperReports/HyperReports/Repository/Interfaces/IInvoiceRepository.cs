﻿using HyperReports.DTOs;
using System;
using System.Collections.Generic;
using System.Text;

namespace HyperReports.Repository.Interfaces
{
    public interface IInvoiceRepository : IBaseRepository<InvoiceDto>
    {
        IEnumerable<InvoiceDto> GetInvoicesByCustomer(int customerId);

        IEnumerable<InvoiceDto> GetInvoicesByStore(int storeId);

        IEnumerable<InvoiceDto> GetInvoicesByCardId(int cardDetailsId);
    }
}