﻿using HyperReports.DTOs;
using HyperReports.Entitites;
using HyperReports.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;

namespace HyperReports.Repository
{
    public class StoreRepository : BaseRepository<StoreDto, StoreEntity>, IStoreRepository
    {
        public StoreRepository(string connection) : base(connection)
        {
        }

        public StoreDto GetStoreByName(string name)
        {
            if (string.IsNullOrEmpty(name))
            {
                throw new ArgumentException("You must provide a name");
            }

            SqlConnection connection = new SqlConnection(_connection);
            string query = "SELECT * FROM Store WHERE name = @name";

            SqlCommand command = connection.CreateCommand();
            command.CommandText = query;
            command.Parameters.AddWithValue("name", name);

            connection.Open();
            using (var reader = command.ExecuteReader())
            {
                if (!reader.Read())
                {
                    return default;
                }

                StoreEntity store = new StoreEntity();
                PopulateModel(store, reader);
                var storeDto = _mapper.Map<StoreDto>(store);
                connection.Close();
                return storeDto;
            }
        }

        public IEnumerable<StoreDto> GetStoresByCompanyId(int companyId)
        {
            if (companyId == default)
            {
                throw new ArgumentNullException("companyId", "Company id must be provided");
            }

            using SqlConnection sqlConnection = new SqlConnection(_connection);
            string query = "SELECT * FROM Store WHERE companyId = @CompanyId";

            var command = sqlConnection.CreateCommand();
            command.CommandText = query;
            command.Parameters.AddWithValue("CompanyId", companyId);

            List<StoreDto> stores = new List<StoreDto>();
            sqlConnection.Open();
            using (var reader = command.ExecuteReader())
            {
                while (reader.Read())
                {
                    StoreEntity store = new StoreEntity();
                    PopulateModel(store, reader);
                    var storeDto = _mapper.Map<StoreDto>(store);
                    stores.Add(storeDto);
                }
            }
            sqlConnection.Close();
            return stores;
        }
    }
}