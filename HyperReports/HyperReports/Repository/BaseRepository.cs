﻿using AutoMapper;
using HyperReports.Attributes;
using HyperReports.AutoMapper;
using HyperReports.DTOs;
using HyperReports.Entitites;
using HyperReports.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Text;

namespace HyperReports.Repository
{
    public abstract class BaseRepository<TDto, TEntity> : IBaseRepository<TDto>
        where TDto : BaseDto, new()
        where TEntity : BaseEntity, new()
    {
        protected readonly string _connection;
        protected readonly IMapper _mapper;

        protected BaseRepository(string connection)
        {
            _connection = connection;
            _mapper = AutoMapperConfig.Configuration().CreateMapper();
        }

        public virtual IEnumerable<TDto> GetAll()
        {
            using SqlConnection connection = new SqlConnection(_connection);
            Type modelType = typeof(TDto);
            string tableName = modelType.Name.Substring(0, modelType.Name.IndexOf("Dto"));

            string query = $"SELECT * FROM {tableName}";

            SqlCommand sqlCommand = connection.CreateCommand();
            sqlCommand.CommandText = query;

            List<TDto> tableData = new List<TDto>();

            connection.Open();
            using (var reader = sqlCommand.ExecuteReader())
            {
                while (reader.Read())
                {
                    TEntity model = new TEntity();
                    PopulateModel(model, reader);
                    var dto = _mapper.Map<TDto>(model);
                    tableData.Add(dto);
                }
            }
            connection.Close();
            return tableData;
        }

        public virtual TDto GetById(int id)
        {
            if (id == default)
            {
                throw new ArgumentNullException("id", "Id must be provided");
            }

            Type modelType = typeof(TDto);
            string tableName = modelType.Name.Substring(0, modelType.Name.IndexOf("Dto"));

            using SqlConnection connection = new SqlConnection(_connection);
            string query = $"SELECT * FROM {tableName} WHERE id = @id";

            SqlCommand sqlCommand = connection.CreateCommand();
            sqlCommand.CommandText = query;
            sqlCommand.Parameters.AddWithValue("@id", id);

            connection.Open();
            using (var reader = sqlCommand.ExecuteReader())
            {
                if (!reader.Read())
                {
                    connection.Close();
                    return default;
                }

                TEntity model = new TEntity();
                PopulateModel(model, reader);
                var dto = _mapper.Map<TDto>(model);
                connection.Close();
                return dto;
            }
        }

        public virtual int Insert(TDto dto)
        {
            var entity = _mapper.Map<TEntity>(dto);
            using SqlConnection connection = new SqlConnection(_connection);
            Type modelType = typeof(TEntity);
            string tableName = modelType.Name.Substring(0, modelType.Name.IndexOf("Entity"));

            IEnumerable<string> columns = modelType
                .GetProperties()
                .Where(prop => !prop.HasAttribute<DoNotInsert>() && !prop.HasAttribute<DoNotPopulate>())
                .Select(prop => prop.Name);

            IEnumerable<string> values = columns.Select(col => $"@{col}");

            string queryColumnsString = string.Join(",", columns);
            string queryValuesString = string.Join(",", values);

            string query = $"INSERT INTO {tableName} ({queryColumnsString}) OUTPUT INSERTED.Id VALUES ({queryValuesString})";

            SqlCommand sqlCommand = connection.CreateCommand();
            sqlCommand.CommandText = query;
            var parameters = GetParameters(entity);
            sqlCommand.Parameters.AddRange(parameters);

            connection.Open();
            var id = (int)sqlCommand.ExecuteScalar();
            connection.Close();
            return id;
        }

        protected SqlParameter[] GetParameters(TEntity model)
        {
            IEnumerable<PropertyInfo> properties = typeof(TEntity)
                .GetProperties()
                .Where(prop => !prop.HasAttribute<DoNotInsert>() && !prop.HasAttribute<DoNotPopulate>());

            List<SqlParameter> parameters = new List<SqlParameter>();

            foreach (var prop in properties)
            {
                SqlParameter parameter = new SqlParameter();
                parameter.ParameterName = $"@{prop.Name}";
                parameter.Value = prop.GetValue(model) == null ? DBNull.Value : prop.GetValue(model);
                parameters.Add(parameter);
            }

            return parameters.ToArray();
        }

        protected virtual void PopulateModel(TEntity model, IDataReader reader)
        {
            IEnumerable<PropertyInfo> properties = typeof(TEntity)
                .GetProperties()
                .Where(prop => !prop.HasAttribute<DoNotPopulate>());

            foreach (var property in properties)
            {
                object value = reader[property.Name];

                if (value is DBNull)
                {
                    value = null;
                }

                property.SetValue(model, value);
            }
        }

        public int GetDbEntityId(TDto dto, string identifier)
        {
            if (dto is null)
            {
                throw new ArgumentException("Entity must be valid");
            }

            if (string.IsNullOrEmpty(identifier))
            {
                throw new ArgumentException("You must provide identifier");
            }

            var entity = _mapper.Map<TEntity>(dto);

            Type entityType = typeof(TEntity);
            string tableName = entityType.Name.Substring(0, entityType.Name.IndexOf("Entity"));

            var identifierValue = entityType.GetProperty(identifier).GetValue(entity);

            SqlConnection connection = new SqlConnection(_connection);
            string query = $"SELECT * FROM {tableName} WHERE {identifier} = @Value";

            SqlCommand command = connection.CreateCommand();
            command.CommandText = query;
            command.Parameters.AddWithValue("Value", identifierValue);

            connection.Open();
            var result = command.ExecuteScalar();

            if (result == null)
            {
                connection.Close();
                return default;
            }
            else
            {
                connection.Close();
                return (int)result;
            }
        }
    }
}