﻿using HyperReports.DTOs;
using HyperReports.Entitites;
using HyperReports.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;

namespace HyperReports.Repository
{
    public class CardDetailsRepository : BaseRepository<CardDetailsDto, CardDetailsEntity>, ICardDetailsRepository
    {
        public CardDetailsRepository(string connection) : base(connection)
        {
        }

        public CardDetailsDto GetCardDetailsByCardNumber(string cardNumber)
        {
            if (string.IsNullOrEmpty(cardNumber))
            {
                throw new ArgumentException("You must provide a card number");
            }

            SqlConnection connection = new SqlConnection(_connection);
            string query = "SELECT * FROM CardDetails WHERE CardNumber = @CardNumber";

            SqlCommand command = connection.CreateCommand();
            command.CommandText = query;
            command.Parameters.AddWithValue("CardNumber", cardNumber);

            connection.Open();
            using (var reader = command.ExecuteReader())
            {
                if (!reader.Read())
                {
                    connection.Close();
                    return default;
                }

                CardDetailsEntity cardDetailsEntity = new CardDetailsEntity();
                PopulateModel(cardDetailsEntity, reader);
                var cardDetailsDto = _mapper.Map<CardDetailsDto>(cardDetailsEntity);
                connection.Close();
                return cardDetailsDto;
            }
        }
    }
}